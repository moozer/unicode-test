import sys
import platform
import io
import locale

print( "Load and show file - testing unicode and such")
print( "- python version:   {}".format( platform.python_version()))
print( "- locale settings:  {}".format( locale.getdefaultlocale()))
print( "- default encoding: {}".format( sys.getdefaultencoding() ))
print( "- stdin and stdout stream encoding: {}, {}".format(sys.stdin.encoding, sys.stdout.encoding) )
print( "Showing content from file {}".format( sys.argv[1]) )
print( "---")
with io.open( sys.argv[1], "rb" ) as fp:
    content = fp.read().decode('utf-8')
    print( "read content is of type {}".format( type(content )))
    print( content )
print("--- EOF")
