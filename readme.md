unicode-test
===============

I'm having issues outputting strings to screen and files when special characters are involved. So I made a test proejct to get it sorted.

We have two file one with ascii and one with unicode. The program reads it and outputs it to screen.

The issue is that it work on the workstation not in gitlab-ci.

Some comments:
* go [here](https://stackoverflow.com/questions/55951685/unicode-error-with-gitlab-ci-not-on-workstation) for a long story of this.
* type "str" in python3 is (mostly) the same as type "unicode" in python2
* When reading and writing to files - use proper encoding/decoding and do it using "rb" or "wb"
* It is a locale issue. adding `export PYTHONIOENCODING=utf8` will help on e.g. docker conatiners will bad locale settings.
